"""Пусть формат записи в лог-файле будет следующим:
TIMESTAMP, OPERATION, EVALUATION TIME(в миллисекундах). Разделитель -- ', '."""
import operator
from functools import reduce
from typing import List


# функция для выделения названия операции и времени исполнения из строки лога
def parse_log_line(log_line: str, separator: str) -> tuple[str, str]:
    splitted_log_line = log_line.split(separator)
    operation_name = splitted_log_line[1]
    evaluation_time = splitted_log_line[2]
    return operation_name, evaluation_time


# функция для вычисления среднего значения времени для операции
def calc_average(
    values: List,
) -> float:
    return reduce(operator.add, values) / len(values)


def process_logs(file_path: str, operations_count: int) -> None:
    # Для хранения информации о логах используется словарь.
    # Ключ -- название функции, значение -- список, содержащий в себе время выполнения функции
    data_dictionary: dict = {}
    try:
        with open(file_path, "r") as log_file:
            # парсинг логов
            for log_line in log_file.readlines():
                operation_name, evaluation_time = parse_log_line(log_line, ", ")
                if operation_name not in data_dictionary.keys():
                    data_dictionary[operation_name] = [
                        float(evaluation_time),
                    ]
                else:
                    data_dictionary[operation_name].append(float(evaluation_time))
    except FileNotFoundError as error:
        print(error)
    for operation_name_key in data_dictionary.keys():
        # вычисление среднего времени выполнения операции
        data_dictionary[operation_name_key] = round(
            calc_average(data_dictionary[operation_name_key]), 2
        )
    # сортировка по времени выполнения в порядке убывания
    sorted_data_dictionary = {
        k: v
        for k, v in sorted(data_dictionary.items(), key=lambda x: x[1], reverse=True)
    }
    for operation_name_key in list(sorted_data_dictionary.keys())[0:operations_count]:
        print(
            f"Evaluation time: {sorted_data_dictionary[operation_name_key]}, Operation name: {operation_name_key}"
        )
